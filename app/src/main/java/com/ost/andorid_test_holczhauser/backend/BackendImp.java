package com.ost.andorid_test_holczhauser.backend;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ost.andorid_test_holczhauser.backend.dao.setresponse.SetResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * This class implements the @link{ #BackendInterface }. Operating with 5000 MS timeout.
 */
public class BackendImp implements BackendInterface {

    private static final String BASE_URL = "http://feature-code-test.skylark-cms.qa.aws.ostmodern.co.uk:8000/";
    private static final String SETS_URL = "api/sets/";
    private static final int MY_SOCKET_TIMEOUT_MS = 5000;

    private final RequestQueue queue;

    public BackendImp(Context context) {
        queue = Volley.newRequestQueue(context);
    }


    @Override
    public void getSetsList(Response.Listener<SetResponse> successListener, Response.ErrorListener errorListener) {

        //TODO: remove debug code
        StringRequest stringRequest = new StringRequest(BASE_URL + "api/sets/coll_e8400ca3aebb4f70baf74a81aefd5a78/items/sche_c061deca120149c983e8c3a49a6b1970/", new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("S", s);
            }
        }, errorListener);

        queue.add(stringRequest);
        //------- END OF DEBUG CODE -------


        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");

        GsonRequest<SetResponse> setsRequest = new GsonRequest<SetResponse>(
                BASE_URL + SETS_URL,
                SetResponse.class, headers,
                successListener,
                errorListener);

        setsRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(setsRequest);
    }


    @Override
    public void getDetails() {

    }
}
