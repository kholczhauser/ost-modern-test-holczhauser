package com.ost.andorid_test_holczhauser.backend;

import com.android.volley.Response;
import com.ost.andorid_test_holczhauser.backend.dao.setresponse.SetResponse;

/**
 * Created by cholczhauser on 15/03/16.
 */
public interface BackendInterface {

    void getSetsList(Response.Listener<SetResponse> listener, Response.ErrorListener errorListener);

    void getDetails();

}
