
package com.ost.andorid_test_holczhauser.backend.dao.setresponse;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetResponse {

    @SerializedName("objects")
    @Expose
    private List<SetResponseObject> objects = new ArrayList<SetResponseObject>();
    @SerializedName("language_version_number")
    @Expose
    private int languageVersionNumber;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("ends_on")
    @Expose
    private String endsOn;
    @SerializedName("version_url")
    @Expose
    private String versionUrl;
    @SerializedName("set_type_slug")
    @Expose
    private String setTypeSlug;

    /**
     * 
     * @return
     *     The objects
     */
    public List<SetResponseObject> getObjects() {
        return objects;
    }

    /**
     * 
     * @param objects
     *     The objects
     */
    public void setObjects(List<SetResponseObject> objects) {
        this.objects = objects;
    }

    /**
     * 
     * @return
     *     The languageVersionNumber
     */
    public int getLanguageVersionNumber() {
        return languageVersionNumber;
    }

    /**
     * 
     * @param languageVersionNumber
     *     The language_version_number
     */
    public void setLanguageVersionNumber(int languageVersionNumber) {
        this.languageVersionNumber = languageVersionNumber;
    }

    /**
     * 
     * @return
     *     The modified
     */
    public String getModified() {
        return modified;
    }

    /**
     * 
     * @param modified
     *     The modified
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     * 
     * @return
     *     The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 
     * @param summary
     *     The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * 
     * @return
     *     The endsOn
     */
    public String getEndsOn() {
        return endsOn;
    }

    /**
     * 
     * @param endsOn
     *     The ends_on
     */
    public void setEndsOn(String endsOn) {
        this.endsOn = endsOn;
    }

    /**
     * 
     * @return
     *     The versionUrl
     */
    public String getVersionUrl() {
        return versionUrl;
    }

    /**
     * 
     * @param versionUrl
     *     The version_url
     */
    public void setVersionUrl(String versionUrl) {
        this.versionUrl = versionUrl;
    }

    /**
     * 
     * @return
     *     The setTypeSlug
     */
    public String getSetTypeSlug() {
        return setTypeSlug;
    }

    /**
     * 
     * @param setTypeSlug
     *     The set_type_slug
     */
    public void setSetTypeSlug(String setTypeSlug) {
        this.setTypeSlug = setTypeSlug;
    }

}
