package com.ost.andorid_test_holczhauser.backend.dao.setresponse;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetResponseObject {

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("schedule_urls")
    @Expose
    private List<String> scheduleUrls = new ArrayList<String>();
    @SerializedName("publish_on")
    @Expose
    private String publishOn;
    @SerializedName("quoter")
    @Expose
    private String quoter;
    @SerializedName("featured")
    @Expose
    private boolean featured;
    @SerializedName("language_modified_by")
    @Expose
    private java.lang.Object languageModifiedBy;
    @SerializedName("plans")
    @Expose
    private List<java.lang.Object> plans = new ArrayList<java.lang.Object>();
    @SerializedName("cached_film_count")
    @Expose
    private int cachedFilmCount;

    @SerializedName("modified_by")
    @Expose
    private java.lang.Object modifiedBy;
    @SerializedName("temp_id")
    @Expose
    private int tempId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("self")
    @Expose
    private String self;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("language_publish_on")
    @Expose
    private String languagePublishOn;
    @SerializedName("language_modified")
    @Expose
    private String languageModified;
    @SerializedName("has_price")
    @Expose
    private boolean hasPrice;
    @SerializedName("set_type_url")
    @Expose
    private String setTypeUrl;
    @SerializedName("temp_image")
    @Expose
    private String tempImage;
    @SerializedName("film_count")
    @Expose
    private int filmCount;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("language_version_url")
    @Expose
    private String languageVersionUrl;
    @SerializedName("quote")
    @Expose
    private String quote;
    @SerializedName("lowest_amount")
    @Expose
    private java.lang.Object lowestAmount;
    @SerializedName("formatted_body")
    @Expose
    private String formattedBody;
    @SerializedName("image_urls")
    @Expose
    private List<java.lang.Object> imageUrls = new ArrayList<java.lang.Object>();
    @SerializedName("hierarchy_url")
    @Expose
    private java.lang.Object hierarchyUrl;
    @SerializedName("schedule_url")
    @Expose
    private String scheduleUrl;
    @SerializedName("active")
    @Expose
    private boolean active;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("version_number")
    @Expose
    private int versionNumber;
    @SerializedName("language_ends_on")
    @Expose
    private String languageEndsOn;
    @SerializedName("created")
    @Expose
    private String created;

    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<Item>();
    @SerializedName("language_version_number")
    @Expose
    private int languageVersionNumber;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("ends_on")
    @Expose
    private String endsOn;
    @SerializedName("version_url")
    @Expose
    private String versionUrl;
    @SerializedName("set_type_slug")
    @Expose
    private String setTypeSlug;
    @SerializedName("content_url")
    @Expose
    private String contentUrl;
    @SerializedName("set_url")
    @Expose
    private String setUrl;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("position")
    @Expose
    private int position;

    /**
     * 
     * @return
     *     The uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * 
     * @param uid
     *     The uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * 
     * @return
     *     The scheduleUrls
     */
    public List<String> getScheduleUrls() {
        return scheduleUrls;
    }

    /**
     * 
     * @param scheduleUrls
     *     The schedule_urls
     */
    public void setScheduleUrls(List<String> scheduleUrls) {
        this.scheduleUrls = scheduleUrls;
    }

    /**
     * 
     * @return
     *     The publishOn
     */
    public String getPublishOn() {
        return publishOn;
    }

    /**
     * 
     * @param publishOn
     *     The publish_on
     */
    public void setPublishOn(String publishOn) {
        this.publishOn = publishOn;
    }

    /**
     * 
     * @return
     *     The quoter
     */
    public String getQuoter() {
        return quoter;
    }

    /**
     * 
     * @param quoter
     *     The quoter
     */
    public void setQuoter(String quoter) {
        this.quoter = quoter;
    }

    /**
     * 
     * @return
     *     The featured
     */
    public boolean isFeatured() {
        return featured;
    }

    /**
     * 
     * @param featured
     *     The featured
     */
    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    /**
     * 
     * @return
     *     The languageModifiedBy
     */
    public java.lang.Object getLanguageModifiedBy() {
        return languageModifiedBy;
    }

    /**
     * 
     * @param languageModifiedBy
     *     The language_modified_by
     */
    public void setLanguageModifiedBy(java.lang.Object languageModifiedBy) {
        this.languageModifiedBy = languageModifiedBy;
    }

    /**
     * 
     * @return
     *     The plans
     */
    public List<java.lang.Object> getPlans() {
        return plans;
    }

    /**
     * 
     * @param plans
     *     The plans
     */
    public void setPlans(List<java.lang.Object> plans) {
        this.plans = plans;
    }

    /**
     * 
     * @return
     *     The cachedFilmCount
     */
    public int getCachedFilmCount() {
        return cachedFilmCount;
    }

    /**
     * 
     * @param cachedFilmCount
     *     The cached_film_count
     */
    public void setCachedFilmCount(int cachedFilmCount) {
        this.cachedFilmCount = cachedFilmCount;
    }

    /**
     * 
     * @return
     *     The modifiedBy
     */
    public java.lang.Object getModifiedBy() {
        return modifiedBy;
    }

    /**
     * 
     * @param modifiedBy
     *     The modified_by
     */
    public void setModifiedBy(java.lang.Object modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * 
     * @return
     *     The tempId
     */
    public int getTempId() {
        return tempId;
    }

    /**
     * 
     * @param tempId
     *     The temp_id
     */
    public void setTempId(int tempId) {
        this.tempId = tempId;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The self
     */
    public String getSelf() {
        return self;
    }

    /**
     * 
     * @param self
     *     The self
     */
    public void setSelf(String self) {
        this.self = self;
    }

    /**
     * 
     * @return
     *     The createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 
     * @param createdBy
     *     The created_by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 
     * @return
     *     The languagePublishOn
     */
    public String getLanguagePublishOn() {
        return languagePublishOn;
    }

    /**
     * 
     * @param languagePublishOn
     *     The language_publish_on
     */
    public void setLanguagePublishOn(String languagePublishOn) {
        this.languagePublishOn = languagePublishOn;
    }

    /**
     * 
     * @return
     *     The languageModified
     */
    public String getLanguageModified() {
        return languageModified;
    }

    /**
     * 
     * @param languageModified
     *     The language_modified
     */
    public void setLanguageModified(String languageModified) {
        this.languageModified = languageModified;
    }

    /**
     * 
     * @return
     *     The hasPrice
     */
    public boolean isHasPrice() {
        return hasPrice;
    }

    /**
     * 
     * @param hasPrice
     *     The has_price
     */
    public void setHasPrice(boolean hasPrice) {
        this.hasPrice = hasPrice;
    }

    /**
     * 
     * @return
     *     The setTypeUrl
     */
    public String getSetTypeUrl() {
        return setTypeUrl;
    }

    /**
     * 
     * @param setTypeUrl
     *     The set_type_url
     */
    public void setSetTypeUrl(String setTypeUrl) {
        this.setTypeUrl = setTypeUrl;
    }

    /**
     * 
     * @return
     *     The tempImage
     */
    public String getTempImage() {
        return tempImage;
    }

    /**
     * 
     * @param tempImage
     *     The temp_image
     */
    public void setTempImage(String tempImage) {
        this.tempImage = tempImage;
    }

    /**
     * 
     * @return
     *     The filmCount
     */
    public int getFilmCount() {
        return filmCount;
    }

    /**
     * 
     * @param filmCount
     *     The film_count
     */
    public void setFilmCount(int filmCount) {
        this.filmCount = filmCount;
    }

    /**
     * 
     * @return
     *     The body
     */
    public String getBody() {
        return body;
    }

    /**
     * 
     * @param body
     *     The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * 
     * @return
     *     The languageVersionUrl
     */
    public String getLanguageVersionUrl() {
        return languageVersionUrl;
    }

    /**
     * 
     * @param languageVersionUrl
     *     The language_version_url
     */
    public void setLanguageVersionUrl(String languageVersionUrl) {
        this.languageVersionUrl = languageVersionUrl;
    }

    /**
     * 
     * @return
     *     The quote
     */
    public String getQuote() {
        return quote;
    }

    /**
     * 
     * @param quote
     *     The quote
     */
    public void setQuote(String quote) {
        this.quote = quote;
    }

    /**
     * 
     * @return
     *     The lowestAmount
     */
    public java.lang.Object getLowestAmount() {
        return lowestAmount;
    }

    /**
     * 
     * @param lowestAmount
     *     The lowest_amount
     */
    public void setLowestAmount(java.lang.Object lowestAmount) {
        this.lowestAmount = lowestAmount;
    }

    /**
     * 
     * @return
     *     The formattedBody
     */
    public String getFormattedBody() {
        return formattedBody;
    }

    /**
     * 
     * @param formattedBody
     *     The formatted_body
     */
    public void setFormattedBody(String formattedBody) {
        this.formattedBody = formattedBody;
    }

    /**
     * 
     * @return
     *     The imageUrls
     */
    public List<java.lang.Object> getImageUrls() {
        return imageUrls;
    }

    /**
     * 
     * @param imageUrls
     *     The image_urls
     */
    public void setImageUrls(List<java.lang.Object> imageUrls) {
        this.imageUrls = imageUrls;
    }

    /**
     * 
     * @return
     *     The hierarchyUrl
     */
    public java.lang.Object getHierarchyUrl() {
        return hierarchyUrl;
    }

    /**
     * 
     * @param hierarchyUrl
     *     The hierarchy_url
     */
    public void setHierarchyUrl(java.lang.Object hierarchyUrl) {
        this.hierarchyUrl = hierarchyUrl;
    }

    /**
     * 
     * @return
     *     The scheduleUrl
     */
    public String getScheduleUrl() {
        return scheduleUrl;
    }

    /**
     * 
     * @param scheduleUrl
     *     The schedule_url
     */
    public void setScheduleUrl(String scheduleUrl) {
        this.scheduleUrl = scheduleUrl;
    }

    /**
     * 
     * @return
     *     The active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     * 
     * @param slug
     *     The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     * 
     * @return
     *     The versionNumber
     */
    public int getVersionNumber() {
        return versionNumber;
    }

    /**
     * 
     * @param versionNumber
     *     The version_number
     */
    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }

    /**
     * 
     * @return
     *     The languageEndsOn
     */
    public String getLanguageEndsOn() {
        return languageEndsOn;
    }

    /**
     * 
     * @param languageEndsOn
     *     The language_ends_on
     */
    public void setLanguageEndsOn(String languageEndsOn) {
        this.languageEndsOn = languageEndsOn;
    }

    /**
     * 
     * @return
     *     The created
     */
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * 
     * @param items
     *     The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * 
     * @return
     *     The languageVersionNumber
     */
    public int getLanguageVersionNumber() {
        return languageVersionNumber;
    }

    /**
     * 
     * @param languageVersionNumber
     *     The language_version_number
     */
    public void setLanguageVersionNumber(int languageVersionNumber) {
        this.languageVersionNumber = languageVersionNumber;
    }

    /**
     * 
     * @return
     *     The modified
     */
    public String getModified() {
        return modified;
    }

    /**
     * 
     * @param modified
     *     The modified
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     * 
     * @return
     *     The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 
     * @param summary
     *     The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * 
     * @return
     *     The endsOn
     */
    public String getEndsOn() {
        return endsOn;
    }

    /**
     * 
     * @param endsOn
     *     The ends_on
     */
    public void setEndsOn(String endsOn) {
        this.endsOn = endsOn;
    }

    /**
     * 
     * @return
     *     The versionUrl
     */
    public String getVersionUrl() {
        return versionUrl;
    }

    /**
     * 
     * @param versionUrl
     *     The version_url
     */
    public void setVersionUrl(String versionUrl) {
        this.versionUrl = versionUrl;
    }

    /**
     * 
     * @return
     *     The setTypeSlug
     */
    public String getSetTypeSlug() {
        return setTypeSlug;
    }

    /**
     * 
     * @param setTypeSlug
     *     The set_type_slug
     */
    public void setSetTypeSlug(String setTypeSlug) {
        this.setTypeSlug = setTypeSlug;
    }

    /**
     * 
     * @return
     *     The contentUrl
     */
    public String getContentUrl() {
        return contentUrl;
    }

    /**
     * 
     * @param contentUrl
     *     The content_url
     */
    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    /**
     * 
     * @return
     *     The setUrl
     */
    public String getSetUrl() {
        return setUrl;
    }

    /**
     * 
     * @param setUrl
     *     The set_url
     */
    public void setSetUrl(String setUrl) {
        this.setUrl = setUrl;
    }

    /**
     * 
     * @return
     *     The contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * 
     * @param contentType
     *     The content_type
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * 
     * @return
     *     The position
     */
    public int getPosition() {
        return position;
    }

    /**
     * 
     * @param position
     *     The position
     */
    public void setPosition(int position) {
        this.position = position;
    }

}
