package com.ost.andorid_test_holczhauser.rootset.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ost.andorid_test_holczhauser.R;
import com.ost.andorid_test_holczhauser.backend.BackendImp;
import com.ost.andorid_test_holczhauser.backend.dao.setresponse.SetResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Displays a default cardview for the user, containing Roots from the backend.
 */
public class MainActivity extends Activity implements Response.Listener<SetResponse>, Response.ErrorListener {

    @Bind(R.id.my_recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.show_episodeButton)
    Button showEpisode;

    @Bind(R.id.refresh_button)
    Button refreshButton;

    @Bind(R.id.materialprogressbar)
    MaterialProgressBar progressBar;

    private BackendImp backend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(getResources().getString(R.string.sets));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        //start downloading
        backend = new BackendImp(getApplicationContext());
        backend.getSetsList(this, this);
    }


    @Override
    public void onResponse(SetResponse setResponse) {
        if (setResponse != null) {
            SetsAdapter mAdapter = new SetsAdapter(setResponse.getObjects());
            recyclerView.setAdapter(mAdapter);
            progressBar.setVisibility(View.GONE);
            showEpisode.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.unknow_error), Toast.LENGTH_SHORT).show();
            Log.e("onResponse", "setResponse Object is NULL");
        }
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_SHORT).show();
            Log.e("onErrorResponse", "Time out error");
        } else if (volleyError != null && volleyError.getMessage() != null) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.download_error) + volleyError.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.unknow_error), Toast.LENGTH_SHORT).show();
            Log.e("onErrorResponse", "volleyError getMessage is NULL");
        }
        progressBar.setVisibility(View.GONE);
        showEpisode.setVisibility(View.GONE);
        refreshButton.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.show_episodeButton)
    public void showEpisodes() {

    }


    @OnClick(R.id.refresh_button)
    public void reconnect() {
        refreshButton.setVisibility(View.GONE);
        backend.getSetsList(this, this);
    }

}
