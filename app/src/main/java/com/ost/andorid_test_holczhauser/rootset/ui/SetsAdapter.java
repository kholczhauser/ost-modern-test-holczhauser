package com.ost.andorid_test_holczhauser.rootset.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ost.andorid_test_holczhauser.R;
import com.ost.andorid_test_holczhauser.backend.dao.setresponse.SetResponseObject;

import java.util.List;

public class SetsAdapter extends RecyclerView.Adapter<SetsAdapter.ViewHolder> {


    private final List<SetResponseObject> dateList;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextView;
        public TextView bodyTextView; //body,quote or summary text. If none of them exists, then default text from resource folder
        public TextView episodeTextView;

        public ViewHolder(View v) {
            super(v);
            titleTextView = (TextView) v.findViewById(R.id.title);
            bodyTextView = (TextView) v.findViewById(R.id.body);
            episodeTextView = (TextView) v.findViewById(R.id.film_count_value);
        }
    }

    public SetsAdapter(List<SetResponseObject> dateList) {
        this.dateList = dateList;
    }

    @Override
    public SetsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SetResponseObject itemToDisplay = dateList.get(position);

        holder.titleTextView.setText(itemToDisplay.getTitle());
        if (itemToDisplay.getItems() != null) {
            holder.episodeTextView.setText(String.valueOf(itemToDisplay.getItems().size()));
        } else {
            holder.episodeTextView.setText(holder.bodyTextView.getContext().getString(R.string.no_episode));
        }


        if (itemToDisplay.getBody() != null && itemToDisplay.getBody().length() > 5) {
            holder.bodyTextView.setText(itemToDisplay.getBody());

        } else if (itemToDisplay.getQuote() != null && itemToDisplay.getQuote().length() > 5) {

            holder.bodyTextView.setText(itemToDisplay.getQuote() + " - " + itemToDisplay.getQuoter());

        } else if (itemToDisplay.getSummary() != null && itemToDisplay.getSummary().length() > 5) {

            holder.bodyTextView.setText(itemToDisplay.getSummary());

        } else {
            holder.bodyTextView.setText(holder.bodyTextView.getContext().getString(R.string.default_body_text));
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dateList.size();
    }
}